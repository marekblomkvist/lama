# Lama
Lama is a changelog manager. It creates a changelog and lets you easily keep track on all changes made. To see an example, visit this package's changelog.

## Install
In order to be used on each project, install Lama globally.
```
npm install @marekblomkvist/lama -g
```

## Usage
### init
This command lets you set up an empty changelog.
```
lama init
```

### change
This command lets you add a change to the current version. All changes are saved under unreleased and when released assign to the new version. There are 6 types of changes: **Added**, **Changed**, **Deprecated**, **Removed**, **Fixed** and **Security**. Just write the choosen type or its corresponding shortform, consisting of its first character. Then you will be prompted to enter the actual change made. After confirmation the change gets added to the unreleased version.
```
lama change [added|changed|deprecated|removed|fixed|security]
lama change [a|c|d|r|f|s]
```
**Example**
This will prompt you for the change made and add it with the type fixed.
```
lama change f
>> Enter your change: Minor bugfixes.
```
Output in changelog: [Fixed] Minor bugfixes.

## release
Once all changes are registered, this command allows you to release a version. You can choose between a **major**, a **minor** or a **patch** release. Then the changelog gets updated and your repository automatically pushed to its remote. By default the remote is **origin**, but there is support for heroku. Just add the flag **-h** to the command.
```
lama release [major|minor|patch] (-h)
```
**Example**
This will update the version to the next minor version and update the changelog. The the repository gets pushed to heroku.
```
lama release minor -h
```

## push
In case you already release your current version, but you want to push it to another one without releasing again. Just use this command and add the **-h** flag for heroku.
```
lama push (-h)
```
