# Changelog
All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.0.2] - 2019-01-15
[Changed] Homepage added to package.json.

## [1.0.1] - 2019-01-15
[Fixed] Force line breaks for changes.  

## [1.0.0] - 2019-01-14
[Added] Readme added.  
[Added] Published to npm.  
[Changed] Scoped package name.  

## [0.5.0] - 2019-01-14
[Added] Push command added. Allows pushing without adding new version. (lama push / lama push -h)  

## [0.4.0] - 2019-01-14
[Added] Heroku support. Choose with -h  

## [0.3.3] - 2019-01-14
[Fixed] Bug in relation with package.json fixed.  

## [0.3.2] - 2019-01-14
[Fixed] Version in package.json gets updated too.  

## [0.3.1] - 2019-01-14
[Fixed] Small bug fix in package.json.  

## [0.3.0] - 2019-01-14
[Changed] Format of changelog changed.  

## [0.2.3] - 2019-01-13
[Fixed] Final .gitignore fix.  

## [0.2.2] - 2019-01-13
[Fixed] .gitignore fix.  

## [0.2.1] - 2019-01-13
[Added] Ideas file added.  
[Fixed] Added .gitignore for node_modules.  

## [0.2.0] - 2019-01-13
[Added] Clear screen multiple times during runtime.  
[Changed] Refactored code.  
[Changed] Simpler log system.  

## [0.1.1] - 2019-01-13
[Changed] Types are now headings instead of bold.  

## [0.1.0] - 2019-01-13
[Added] Basic functionality is working.  
