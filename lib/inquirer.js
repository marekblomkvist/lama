const inquirer = require('inquirer');

module.exports = {
  askChange: () => {
    const questions = [
      {
        name: 'content',
        type: 'input',
        message: 'Enter change:',
        validate: (value) => {
          if (value.length) {
            return true;
          } else {
            return 'Please enter change';
          }
        }
      }
    ];
    return inquirer.prompt(questions);
  },
  askConfirmation: () => {
    const questions = [
      {
        name: 'confirmation',
        type: 'confirm',
        message: 'Check the changes. If correct continue.'
      }
    ];
    return inquirer.prompt(questions);
  }
}
