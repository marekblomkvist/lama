const fs = require('fs');
const path = require('path');

module.exports = {
  getCurrentDirectoryBase: () => {
    return path.basename(process.cwd());
  },
  directoryExists: (filePath) => {
    try {
      return fs.statSync(filePath).isDirectory();
    } catch (err) {
      return false;
    }
  },
  fileExists: (filePath) => {
    try {
      return fs.statSync(filePath).isFile();
    } catch (err) {
      return false;
    }
  },
  readFile: (filePath) => {
    return fs.readFileSync(filePath, {encoding: 'utf-8'});
  },
  writeFile: (filePath, content) => {
    fs.writeFile(filePath, content, function (err) {
      if (err) throw err;
    });
  }
}
