const chalk = require('chalk');
const figlet = require('figlet');
const git =  require('simple-git')();
const CLI = require('clui')
const Spinner = CLI.Spinner;

const files = require('./files');
const inquirer = require('./inquirer');

class Lama {

  static clear() {
    process.stdout.write('\x1b[2J');
  }

  // cmd: inititalize Lama
  static cmdInitialize() {
    if (files.fileExists('changelog.md')) {
      Lama.log('error', 'Already initalized.');
      process.exit();
    }

    let content = '# Changelog\nAll notable changes to this project will be documented in this file.\n\n## [Unreleased]\n';
    files.writeFile('changelog.md', content);

    Lama.clear();
    Lama.logo();
    Lama.log('success', 'Changelog initialized.');
  }

  // cmd: add to changelog
  static async cmdChange(argument) {
    Lama.isInitialized();

    const inputs = {
      'a': 'A',
      'added': 'A',
      'c': 'C',
      'changed': 'C',
      'd': 'D',
      'deprecated': 'D',
      'r': 'R',
      'removed': 'R',
      'f': 'F',
      'fixed': 'F',
      's': 'S',
      'security': 'S',
    }
    const types = {
      'A': 'Added',
      'C': 'Changed',
      'D': 'Deprecated',
      'R': 'Removed',
      'F': 'Fixed',
      'S': 'Security'
    };
    let type = (inputs[argument] !== undefined) ? inputs[argument] : 'A';

    const input = await inquirer.askChange();
    const content = input.content;
    const changelog = files.readFile('changelog.md');
    let versions = changelog.split(/(\n\#{2}\s)/g);
    const unreleased = versions[2];

    // split unreleased by lines
    const unreleasedLines = unreleased.split('\n');
    let unreleasedGrouped = {
      'A': [],
      'C': [],
      'D': [],
      'R': [],
      'F': [],
      'S': []
    };

    // assign lines to group except first and last
    for (let i = 1; i < (unreleasedLines.length - 1); i++) {
      unreleasedGrouped[unreleasedLines[i].substring(1,2)].push(unreleasedLines[i]);
    }

    // add new change
    let item = '[' + types[type] + '] ' + content + '  '
    unreleasedGrouped[type].push(item);

    let unreleasedNewLines = [];

    // combine all groups into 1 array
    unreleasedNewLines = unreleasedNewLines.concat(unreleasedGrouped['A'])
      .concat(unreleasedGrouped['C'])
      .concat(unreleasedGrouped['D'])
      .concat(unreleasedGrouped['R'])
      .concat(unreleasedGrouped['F'])
      .concat(unreleasedGrouped['S']);

    // add back first and last line
    unreleasedNewLines.unshift('[Unreleased]');
    unreleasedNewLines.push('');

    // convert to string
    const unreleasedNew = unreleasedNewLines.join('\n');

    // put back in original
    versions[2] = unreleasedNew;
    let fileNew = versions.join('');
    files.writeFile('changelog.md', fileNew);

    Lama.clear();
    Lama.logo();
    Lama.log('success', '[' + types[type] + '] ' + content);
  }

  // release version
  static async cmdRelease(argument, remote) {
    Lama.isInitialized();

    let changelog = files.readFile('changelog.md');
    let versions = changelog.split(/(\n\#{2}\s)/g);
    let unreleased = versions[2];

    // get latest version
    let oldVersion = (versions.length > 4) ? Lama.getVersion(versions[4]) : '0.0.0';

    // get new version
    let newVersion = oldVersion.split('.');
    switch (argument) {
      case 'major':
        newVersion[0] = parseInt(newVersion[0]) + 1;
        newVersion[1] = 0;
        newVersion[2] = 0;
        break;
      case 'minor':
        newVersion[1] = parseInt(newVersion[1]) + 1;
        newVersion[2] = 0;
        break;
      case 'patch':
        newVersion[2] = parseInt(newVersion[2]) + 1;
        break;
    }
    newVersion = newVersion.join('.');


    let date = Lama.getDate(new Date());
    unreleased = unreleased.replace(/\[([a-z]*)\]/i, '['+newVersion+'] - ' + date);

    // ask for confirmation
    console.log(unreleased);
    const input = await inquirer.askConfirmation();

    if (input.confirmation) {
      // add empty unreleased
      unreleased = '[Unreleased]\n\n## ' + unreleased;
      versions[2] = unreleased;

      let fileNew = versions.join('');
      files.writeFile('changelog.md', fileNew);
      Lama.updateVersion(newVersion);
      Lama.clear();
      Lama.logo();
      Lama.log('success', 'Changelog updated.');

      if (!files.directoryExists('.git')) {
        Lama.clear();
        Lama.logo();
        Lama.log('error', 'Not a git repository.');
        process.exit();
      }

      Lama.clear();
      Lama.logo();
      const status = new Spinner('Pushing to ' + remote + ' master...');
      status.start();

      try {
        await git
          .add('./*')
          .commit(newVersion)
          .push(remote, 'master');
        return true;
      } catch(err) {
        Lama.log('error', err);
      } finally {
        status.stop();
        Lama.clear();
        Lama.logo();
        Lama.log('success', '[' + newVersion + '] Repository pushed to ' + remote + ' master.');
      }
    }
  }

  static async cmdPush(remote) {
    Lama.isInitialized();

    if (!files.directoryExists('.git')) {
      Lama.clear();
      Lama.logo();
      Lama.log('error', 'Not a git repository.');
      process.exit();
    }

    const changelog = files.readFile('changelog.md');
    const version = Lama.getVersion(changelog);

    Lama.clear();
    Lama.logo();
    const status = new Spinner('Pushing to ' + remote + ' master...');
    status.start();

    try {
      await git.push(remote, 'master');
      return true;
    } catch (err) {
      Lama.log('error', err);
    } finally {
      status.stop();
      Lama.clear();
      Lama.logo();
      Lama.log('success', '[' + version + '] Repository pushed to ' + remote + ' master.');
    }
  }

  static async updateVersion(version) {
    const file = files.readFile('package.json');

    let object = JSON.parse(file);
    object.version = version;

    files.writeFile('package.json', JSON.stringify(object));
  }

  static getDate(date) {
    var dd = date.getDate();
    var mm = date.getMonth() + 1;
    var yyyy = date.getFullYear();

    dd = (dd < 10) ? '0' + dd : dd;

    mm = (mm < 10) ? '0' + mm : mm;

    date = yyyy + '-' + mm + '-' + dd;
    return date;
  }

  static getVersion(input) {
    return input.match(/\[([0-9]*\.[0-9]*\.[0-9]*)\]/)[1];
  }

  static isInitialized() {
    if (!files.fileExists('changelog.md')) {
      Lama.log('error', 'Not yet initalized.');
      process.exit();
    }
  }

  static log(type, message) {
    switch (type) {
      case 'success':
        console.log(chalk.green('Success: ') + chalk.white(message));
        break;
      case 'error':
        console.log(chalk.red('Error: ') + chalk.white(message));
        break;
    }
  }

  static logo() {
    console.log(
      chalk.magenta(
        figlet.textSync('Lama', { horizontalLayout: 'full' })
      )
    );
  }

  static run() {
    Lama.clear();
    Lama.logo();
    const argv = require('minimist')(process.argv.slice(2));
    const remote = (argv['h'] === true) ? 'heroku' : 'origin';

    switch (argv['_'][0]) {
      case 'init':
        Lama.cmdInitialize();
        break;
      case 'change':
        Lama.cmdChange(argv['_'][1]);
        break;
      case 'release':
        Lama.cmdRelease(argv['_'][1], remote);
        break;
      case 'push':
        Lama.cmdPush(remote);
        break;
    }
  }
}

module.exports = Lama;
